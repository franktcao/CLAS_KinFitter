#include "collect_coh_dvcs.h"
R__LOAD_LIBRARY(libFCKinFitter)

void collect_coh_dvcs( double clc = 0.4 ){
  // To select the events passing the confidence level cut, we want events with confidence levels between the cut and 1
  double clc_left  = clc;
  double clc_right = 1;
  
  // If clc is negative, we want the failed events
  if( clc < 0 ){ clc_right = -clc; clc_left = 0;  }
  
  // Define the problem  //////////////////////////////////////////////////////////////////////////////////////////////////
  //// Define KFPs to help evaluate constraints and derivatives
  int is_measured = 1;
  std::vector<KFParticle> KFPs;
  KFPs.emplace_back( "e"            , is_measured = 1 ); // KFParticle ctor is initiated with the name and whether it is measured (1) or not (0)
  KFPs.emplace_back( "^{4}He"       , is_measured = 1 ); // KFParticle ctor is initiated with the name and whether it is measured (1) or not (0)
  KFPs.emplace_back( "#gamma_{DVCS}", is_measured = 1 ); // KFParticle ctor is initiated with the name and whether it is measured (1) or not (0)
  
  //// Tie the elements of KFPs to individual particles that will update from event to event
  KFParticle &KFP_e     = KFPs[0];
  KFParticle &KFP_rec   = KFPs[1];
  KFParticle &KFP_prod  = KFPs[2];
  
  //// Set up the constraint functions
  TLorentzVector P_beam = TLorentzVector( 0, 0, 6.064, 6.064  );
  TLorentzVector P_targ = TLorentzVector( 0, 0, 0    , 3.7274 );
  TLorentzVector P_init = P_beam + P_targ;
  
  ////// Useful shorthand alias
  using vector_d = std::vector<double>;
  ////// Capture P_init to evaluate constraint
  auto eval_constraints 
    = [&](const vector_d &_y, const vector_d &_x){ 
      // Returns evaluated constraints of exclusivity 
      //   from measured (_y) and unmeasured (_x) variables

      // Final state only involves the first 3 particles
      TLorentzVector P_fin;
      std::for_each( KFPs.begin(), KFPs.end(), [&](KFParticle &kfp){ P_fin += kfp.GetP(_y, _x); } );

      TLorentzVector P_tot = P_init - P_fin;
      int n_constraints = 4;
      vector_d result(n_constraints, 0);
      for( int ii = 0; ii < n_constraints; ii++ ) result.at(ii) = P_tot[ii]; 
      return result;
    }; 

  //// This should set up the kin. fit problem completely. Now we only need to update the KFParticle each event and feed it to kin. fitter

  //// Initialize Kin. fitter to initialize kin. fit monitor
  KinFitter fit   = KinFitter( KFPs, eval_constraints );
  KFMonitor kfmon = KFMonitor( fit, clc, P_beam, P_targ );
  // Define the problem XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


  // IO ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //// (After "Define the problem" so that not all those histograms are written into the output file)
  //// In ////////////////////////////////////////////////////////////////////////////
  TFile *f_in = new TFile( "dataIn/eg6_coh_dvcs_pass_fit_plus_3000.root" , "read" );
  TTree *t_in = (TTree*)gROOT->FindObject("eg6_coh_dvcs");
  if( t_in == 0 ){ cout << "Dead tree " << endl; return; }
  t_in->SetBranchAddress("helicity",  &hel   );
  t_in->SetBranchAddress("vz_e",      &vz_e  );
  t_in->SetBranchAddress("P_e",       &P_e   );
  t_in->SetBranchAddress("P_rec",     &P_rec );
  t_in->SetBranchAddress("P_prod",    &P_prod);
  t_in->SetBranchAddress("detID",     &detID );
  //// Out ///////////////////////////////////////////////////////////////////////////
  TFile *f_out = new TFile( Form("root/coh_dvcs_passclc_%.3f.root", clc), "recreate" );
  TTree * t_out = new TTree( "eg6_coh_dvcs", "EG6 Coherent DVCS" );
  t_out = (TTree*) t_in->CloneTree(0);
  t_out->Branch( "Q2",          &Q2         );
  t_out->Branch( "x",           &x          );
  t_out->Branch( "t",           &t          );
  t_out->Branch( "phi",         &phi        );
  t_out->Branch( "confLevel",   &confLevel  );
  t_out->Branch( "P_e_fit",     &P_e_fit    );
  t_out->Branch( "P_rec_fit",   &P_rec_fit  );
  t_out->Branch( "P_prod_fit",  &P_prod_fit );
  t_out->Branch( "Q2_fit",      &Q2_fit     );
  t_out->Branch( "x_fit",       &x_fit      );
  t_out->Branch( "t_fit",       &t_fit      );
  t_out->Branch( "phi_fit",     &phi_fit    );
  t_out->Branch( "kinfit",      &fit        );
  t_out->Branch( "confLevel",   &confLevel  );
  // IO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


  // Loop over events  //////////////////////////////////////////////////////////////////////////////////////////////////
  Int_t nevents = t_in->GetEntries();
  for( Int_t ientry = 0; ientry < nevents; ientry++){
    t_in->GetEntry(ientry);
    printProgress( ientry, nevents );
    
    // Update Lorentz Vectors for each event

    KFP_e   .SetP( *P_e     );
    KFP_rec .SetP( *P_rec   );
    KFP_prod.SetP( *P_prod  );

    // Update sigmas (can be skipped if covariance matrices are readily available)
    vector_d sigs_e    = getResErr_DC  ( *P_e                 );
    vector_d sigs_rec  = getResErr_RTPC( *P_rec               );
    vector_d sigs_prod = getPhotErrors ( detID, *P_prod, vz_e );

    // Update covariance matrices
    KFP_e   .SetCovMat_sigmas( sigs_e    );
    KFP_rec .SetCovMat_sigmas( sigs_rec  );
    KFP_prod.SetCovMat_sigmas( sigs_prod );
    
    // Fit
    fit.UpdateKFParticles( KFPs );
    fit.ProcessFit(); 
   
    // Skip events that do not converge
    if( fit.exit_status == -1 ) continue; 

    
    // Get results from fit
    confLevel =  fit.confLevel;
    P_e_fit    = &fit.KFParticlesOut[0].P;
    P_rec_fit  = &fit.KFParticlesOut[1].P;
    P_prod_fit = &fit.KFParticlesOut[2].P;

    //// Passing Confidence Level Cut
    // Fill histograms
    kfmon.FillHistograms( fit );

    //// Passing Confidence Level Cut
    if( clc_left <= confLevel && confLevel <= clc_right ) t_out->Fill(); 
    
  }
  // Loop over events XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  kfmon.DrawAll();
  f_out->Write();


}

