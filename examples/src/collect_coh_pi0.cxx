#include "collect_coh_pi0.h"
R__LOAD_LIBRARY(libFCKinFitter)

void collect_coh_pi0( double clc = 0.05 ){
    // To select the events passing the confidence level cut, we want events with confidence levels between the cut and 1
    double clc_left  = clc;
    double clc_right = 1;

    // if clc is negative, we want the failed events
    if( clc < 0 ){ clc_right = -clc; clc_left = 0;  }

    // Define the problem  //////////////////////////////////////////////////////////////////////////////////////////////////

    //// Define KFPs to help evaluate constraints and derivatives
    ////// KFParticle ctor is initiated with the name and whether it is measured (1) or not (0)
    std::vector<KFParticle> KFPs;
    int is_measured = 1;
    KFPs.emplace_back( "e",          is_measured = 1 ); 
    KFPs.emplace_back( "^{4}He",     is_measured = 1 ); 
    KFPs.emplace_back( "#pi^{0}",    is_measured = 0 ); 
    KFPs.emplace_back( "#gamma_{1}", is_measured = 1 ); 
    KFPs.emplace_back( "#gamma_{2}", is_measured = 1 ); 

    ////// Tie the elements of KFPs to individual particles that will update from event to event
    KFParticle &KFP_e      = KFPs[0];
    KFParticle &KFP_rec    = KFPs[1];
    KFParticle &KFP_prod   = KFPs[2];
    KFParticle &KFP_phot1  = KFPs[3];
    KFParticle &KFP_phot2  = KFPs[4];

    //// Set up the constraint functions
    ////// Capture P_init to evaluate constraint
    TLorentzVector P_beam = TLorentzVector( 0, 0, 6.064, 6.064  );
    TLorentzVector P_targ = TLorentzVector( 0, 0, 0    , 3.7274 );
    TLorentzVector P_init = P_beam + P_targ;

    ////// Useful shorthand alias
    using vector_d = std::vector<double>;

    //// 1. Exclusivity constraints: e he4 -> e he4 pi0
    auto exc_constraints = [&](const vector_d &_y, const vector_d &_x){ 
      // Returns evaluated constraints of exclusivity 
      //   from measured (_y) and unmeasured (_x) variables

      // Final state only involves the first 3 particles
      TLorentzVector P_fin;
      std::for_each( KFPs.begin(), KFPs.begin()+3, [&](KFParticle &kfp){ P_fin += kfp.GetP(_y, _x); } );

      TLorentzVector P_tot = P_init - P_fin;
      int n_constraints = 4;
      vector_d result(n_constraints, 0);
      for( int ii = 0; ii < n_constraints; ii++ ) result.at(ii) = P_tot[ii]; 
      return result;
    }; 

    //// 2. Decay constraints: pi0 -> gamma gamma
    auto dec_constraints = [&]( const vector_d &_y, const vector_d &_x ){ 
      // Returns evaluated constraints of pi0 decay 
      //   from measured (_y) and unmeasured (_x) variables

      // Final state only involves the last 2 particles
      TLorentzVector P_daughters;
      std::for_each( KFPs.end()-2, KFPs.end(), [&]( KFParticle &kfp ){ P_daughters += kfp.GetP(_y, _x); } );

      TLorentzVector P_parent = KFP_prod.GetP( _y, _x );
      TLorentzVector P_tot    = P_parent - P_daughters;
      int n_constraints = 4;
      vector_d result(n_constraints, 0);
      for( int ii = 0; ii < n_constraints; ii++ ) result.at(ii) = P_tot[ii]; 
      return result;
    }; 

    //// Collect constraints functions 
    std::vector< std::function<vector_d( const vector_d&, const vector_d& ) > > set_of_constraints; 
    set_of_constraints.push_back( exc_constraints );
    set_of_constraints.push_back( dec_constraints );

    
    // Initialize Kin. fitter to initialize kin. fit monitor
    KinFitter fit   = KinFitter( KFPs, set_of_constraints );
    KFMonitor kfmon = KFMonitor( fit, clc, P_beam, P_targ );

    // This should set up the kin. fit problem completely. Now we only need to update the KFParticle each event and feed it to kin. fitter
    // Define the problem XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    // IO //////////////////////////////////////////////////////////////////////////////////////////////////
    //// (After "Define the problem" so that not all those histograms are written into the output file)
    //// In ////////////////////////////////////////////////////////////////////////////
    TFile *f_in = new TFile( "dataIn/eg6_coh_pi0_pass_cutsOrFit.root" , "read" );
    TTree *t_in = (TTree*)gROOT->FindObject("eg6_coh_pi0");
    if( t_in == 0 ){ cout << "Dead tree \n"; return; }
    int eventid = 0;
    t_in->SetBranchAddress("helicity",  &hel     );
    t_in->SetBranchAddress("vz_e",      &vz_e    );
    t_in->SetBranchAddress("P_e",       &P_e     );
    t_in->SetBranchAddress("P_rec",     &P_rec   );
    t_in->SetBranchAddress("P_prod",    &P_prod  );
    t_in->SetBranchAddress("P_phot1",   &P_phot1 );
    t_in->SetBranchAddress("P_phot2",   &P_phot2 );
    t_in->SetBranchAddress("detID",     &detID   );
    t_in->SetBranchAddress("detID1",    &detID1  );
    t_in->SetBranchAddress("detID2",    &detID2  );
    t_in->SetBranchAddress("evntid",    &eventid );
    //// Out ///////////////////////////////////////////////////////////////////////////
    TFile *f_out = new TFile( Form("root/coh_pi0_passclc_%.3f.root", clc ), "recreate" );
    TTree * t_out = new TTree( "eg6_coh_pi0", "EG6 Coherent DVCS" );
    t_out = (TTree*) t_in->CloneTree(0);
    t_out->Branch( "Q2",          &Q2         );
    t_out->Branch( "x",           &x          );
    t_out->Branch( "t",           &t          );
    t_out->Branch( "phi",         &phi        );
    t_out->Branch( "P_e_fit",     &P_e_fit    );
    t_out->Branch( "P_rec_fit",   &P_rec_fit  );
    t_out->Branch( "P_prod_fit",  &P_prod_fit );
    t_out->Branch( "Q2_fit",      &Q2_fit     );
    t_out->Branch( "x_fit",       &x_fit      );
    t_out->Branch( "t_fit",       &t_fit      );
    t_out->Branch( "phi_fit",     &phi_fit    );
    t_out->Branch( "kinfit",      &fit        );
    // IO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


    // Loop over events /////////////////////////////////////////////////////////////////////////////////////
    Int_t nevents = t_in->GetEntries();
    for( Int_t ientry = 0; ientry < nevents; ientry++){
      t_in->GetEntry(ientry);
      printProgress( ientry, nevents );

      // Set up the unmeasured pi0
      TLorentzVector P_X_pi0 = *P_phot1 + *P_phot2;
      P_X_pi0.SetVectM( P_X_pi0.Vect(), 0.1349766 );

      // Update Lorentz Vectors for each event
      KFP_e    .SetP( *P_e      );
      KFP_rec  .SetP( *P_rec    );
      KFP_prod .SetP( P_X_pi0   );
      KFP_phot1.SetP( *P_phot1  );
      KFP_phot2.SetP( *P_phot2  );

      // Update sigmas (can be skipped if covariance matrices are readily available)
      vector_d sigs_e     = getResErr_DC  ( *P_e                   );
      vector_d sigs_rec   = getResErr_RTPC( *P_rec                 );
      vector_d sigs_phot1 = getPhotErrors ( detID1, *P_phot1, vz_e );
      vector_d sigs_phot2 = getPhotErrors ( detID2, *P_phot2, vz_e );

      // Update covariance matrices
      KFP_e    .SetCovMat_sigmas( sigs_e     );
      KFP_rec  .SetCovMat_sigmas( sigs_rec   );
      KFP_phot1.SetCovMat_sigmas( sigs_phot1 );
      KFP_phot2.SetCovMat_sigmas( sigs_phot2 );

      // Fit
      fit.UpdateKFParticles( KFPs );
      fit.ProcessFit(); 

      // Skip events that do not converge
      if( fit.exit_status == -1 ) continue; 

      // Get results from fit
      confLevel   =  fit.confLevel;
      P_e_fit     = &fit.KFParticlesOut[0].P;
      P_rec_fit   = &fit.KFParticlesOut[1].P;
      //P_prod_fit = &fit.KFParticlesOut[2].P;
      P_phot1_fit = &fit.KFParticlesOut[3].P;
      P_phot2_fit = &fit.KFParticlesOut[4].P;
      *P_prod_fit = *P_phot1_fit + *P_phot2_fit;

      // Fill 
      kfmon.FillHistograms( fit );

      //// Select Events Passing The Confidence Level Cut
      if( clc_left <= confLevel && confLevel <= clc_right ){ t_out->Fill(); }

    }
    // Loop over events XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    f_out->Write();
    kfmon.DrawAll();
  }


