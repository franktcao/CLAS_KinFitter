#ifndef collect_coh_pi0_h
#define collect_coh_pi0_h

#include <FCKinFitter/KinFitter.h>
#include <FCKinFitter/KFMonitor.h>
#include <FCKinFitter/Kinematics.h>
#include <FCKinFitter/res-errs_clas.h>
#include "utilities/util.h"

int             hel         = 0;        // Helicity
int             detID       = 0;        // Detector ID (1: EC, 2: IC)
int             detID1      = 0;        // Detector ID (1: EC, 2: IC)
int             detID2      = 0;        // Detector ID (1: EC, 2: IC)
double          vz_e        = 0;        // Electron Vertex's Z-component
TLorentzVector *P_e         = nullptr;  // Scattered electron
TLorentzVector *P_rec       = nullptr;  // Recoiled particle: Helium 
TLorentzVector *P_prod      = nullptr;  // Produced particle: pi0 photon
TLorentzVector *P_phot1     = nullptr;  // Produced particle: pi0 photon
TLorentzVector *P_phot2     = nullptr;  // Produced particle: pi0 photon


double          confLevel   = 0;        // Confidence level from fit
TLorentzVector *P_e_fit     = nullptr;  // Fitted Scattered electron
TLorentzVector *P_rec_fit   = nullptr;  // Fitted Recoiled particle: Helium 
TLorentzVector *P_prod_fit  = nullptr;  // Fitted Produced particle: pi0 photon
TLorentzVector *P_phot1_fit = nullptr;  // Fitted Produced particle: pi0 photon
TLorentzVector *P_phot2_fit = nullptr;  // Fitted Produced particle: pi0 photon

// Kinematic Variables Q2, x, t, and phi
std::vector<double> kinvars    (4); 
std::vector<double> kinvars_fit(4);
double &Q2      = kinvars[0];
double &x       = kinvars[1];
double &t       = kinvars[2];
double &phi     = kinvars[3];
double &Q2_fit  = kinvars_fit[0];
double &x_fit   = kinvars_fit[1];
double &t_fit   = kinvars_fit[2];
double &phi_fit = kinvars_fit[3];


#endif
