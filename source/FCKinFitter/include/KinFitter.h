#ifndef KinFitter_h
#define KinFitter_h
#include <iostream>
#include <functional>
#include <TLorentzVector.h>
#include "KFParticle.h"
//#include <dollar/dollar.hpp>


/** KinFitter
 *    KinFitter applies kinematic fit to a set of particles, KFParticlesIn, using a set of constraint functions, set_of_constraints. 
 *    You can use it by simply feeding in the two std::vectors in your event loop:
 *
 *        std::vector<KFParticle> Particles;
 *        ...
 *        std::vector< std::function<double( const VectorXd&, const VectorXd& )> > constraints;
 *        ...
 *        KinFitter KinFitter( Particles, constraints );
 *
 *    This will apply the fit and from this you can access the new fitted particles, confidence level, and pulls: 
 *
 *        double conflevel = KinFitter.confLevel;
 *        std::vector<double> pulls = KinFitter.pulls;
 *        std::vector<KFParticle> Particles_fitted = KinFitter.KFParticlesOut;
 *
 */

// Useful aliases
using std::cout;
using std::endl;

using vector_d     = std::vector<double>;
using vector_d2    = std::vector< vector_d >;
using vector_kfp   = std::vector<KFParticle>;
using constraint_t = std::function<vector_d(const vector_d&, const vector_d& )>;
using vector_func  = std::vector<constraint_t>;

class KinFitter{


  public:

  double        confLevel; 
  vector_d      pulls; 
  vector_kfp    KFParticlesOut;

  vector_kfp    KFParticlesIn;
  
  //vector_func   set_of_constraints;
  
  std::vector< std::function<std::vector<double>(const std::vector<double>&, const std::vector<double>& )> >  set_of_constraints; 
  // root needs the type of all data members' of a class to be spelled out in full

  bool          isValid = false;
  bool          isCovMatDiag = true;   // Is the covariance matrix diagonal?
  int           n_constraints;
  int           ndf;
  int           iter;
  double        chi2; 
  int           n_chi2_increases;     // Number of times chi2 increases
  int           exit_status = 0;      // 0: no fit, 1: chi2 minimized, -1: confLev < 1E-8, -2:  n_chi2 increases, -3: reached cap

  vector_d      eta; 
  vector_d      x0; 
  vector_d      epsilon; 
  vector_d      sigma2s_epsilon;
  vector_d      cov_mat_compact;
  vector_d2     C_eta;
  


  public:
  
  KinFitter( ){ };
  KinFitter( vector_kfp &Particles, const constraint_t &constraints_functs  );
  KinFitter( vector_kfp &Particles, const vector_func  &_set_of_constraints );
  virtual ~KinFitter(){};

  void       SetCovarianceMatrix( const vector_d &_covMat, bool _isDiag = false );
  void       UpdateKFParticles( vector_kfp &_KFPsIn );
  void       ProcessFit( int nter = 25, double cl_thresh = 1E-8 );
  void       AddParticle( KFParticle &Part );
  int        GetNPart( );
  int        GetNMeasured( );
  int        GetNUnmeasured( );
  vector_d   GetUnmeasured( int ifit = 0 );
  vector_d   GetMeasured( int ifit = 0 );
  
  private:

  void       InitParticles( vector_kfp &_KFParticles );
  bool       isReiterate( vector_d &delta, double cl_thresh );
  void       ExtendCovMat( vector_d &_cov_mat, bool _isCovMatDiag );
  void       SetFittedKFParticles( const vector_d &y, const vector_d &x );
  void       SetConfLevel( );
  void       SetPulls( );
  void       StageOutputs( );
  double     CalcChi2( vector_d &delta );
  vector_d   GetFlattenedCovMat( );
  vector_d2  Get2DCovMat();                                                   // Get 2D matrix of covariance matrix

  std::tuple< vector_d, vector_d, vector_d, double > GetNewDeltaXiAndErrors( vector_d &y, vector_d &x );

};

#endif




