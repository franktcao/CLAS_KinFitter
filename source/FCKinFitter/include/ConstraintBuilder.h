#ifndef ConstraintBuilder_h
#define ConstraintBuilder_h
#include <functional>

template<typename Constraints>
class ConstraintBuilder{

  using std::vector<double>;
  using std::function<vector<double>(const vector<double>&, const vector<double>&, int, int )>
  
  public:
    int                          n_constraints;
    vector<double>               c; 
    vector<vector<double>>       B; 
    vector<vector<double>>       A; 
    function< vector<double>(const vector<double>&, const vector<double>&, int, int )> eval_constraints;

    constraints<typename std::decay<Constraints>::type> build_constraints(Constraints&& c){ return { std::forward<Constraints>(c) }; }
  
  public:
    virtual ~ConstraintBuilder(){};
    ConstraintBuilder( ){ };
    ConstraintBuilder( const function<vector<double>(const vector<double>&, const vector<double>&, int, int )>  &constraints_functs, int ncons );
    std::tuple< vector<double>, vector<vector<double>>, vector<vector<double>> > GetCBA( const vector<double> &y, const vector<double> &x );
};

#endif
