#ifndef Kinematics_h
#define Kinematics_h
#include <iostream>
//#include <functional>
#include <string>

#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include "TCanvas.h"
#include "TLine.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TObject.h"
#include "TMath.h"
#include "KinFitter.h"

// Useful aliases
using std::cout;
using std::endl;
using std::string;

using vector_d     = std::vector<double>;

class Kinematics : public TObject{
  
  public:

    TLorentzVector P_beam;                    // 4-momentum of beam
    TLorentzVector P_targ;                    // 4-momentum of target
    
    TLorentzVector P_scat;                    // 4-momentum of scattered beam (scattered electron)
    TLorentzVector P_rec;                     // 4-momentum of recoiled target  
    TLorentzVector P_prod;                    // 4-momentum of produced particle (dvcs photon, pi0, etc)
    TLorentzVector P_spec;                    // 4-momentum of spectator 
    
    TLorentzVector P_q;                       // 4-momentum of virtual photon
  
    double Q2 ;                               // Q^2 or virtuality of the virtual photon           [ (GeV/c)^2 ]
    double x  ;                               // Bjorken x                                         [ ]
    double t  ;                               // Mandelstam invariant momentum transfer variable t [ (GeV/c)^2 ]
    double phi;                               // Angle between leptonic and hadronic planes        [ rad ]

    struct ExclusivityVariables{
      TLorentzVector P_init;                  // 4-momentum of total initial state
      TLorentzVector P_q;                     // 4-momentum of virtual photon
      double      M2_X_prod;                  // Squared missing mass of produced particle  
      double      M2_X_rec;                   // Squared missing mass of recoiled particle      
      double      M2_X;                       // Squared missing mass of the collection      
      double      px_X;                       // Missing x- momentum of the collection  
      double      py_X;                       // Missing y- momentum of the collection  
      double      pt_X;                       // Missing transverse momentum of the collection  
      double      E_X;                        // Missing energy of the collection    
      double      theta_X_rec;                // Opening angle between missing and measured recoil        
      double      dphi_rec_prod;              // Angle between hadronic planes from by target&&recoil and produced&&virtual photon          
      void        SetExclusivityVariables( const TLorentzVector &_beam, const TLorentzVector &_targ, const TLorentzVector &_scat, 
                                           const TLorentzVector &_rec, const TLorentzVector &_prod, const TLorentzVector &_spec );
                                           // Set the exclusivity variables from kinematic variables (from 4-momenta)
      double      GetCoplanarity( const TLorentzVector &P_rec, const TLorentzVector &P_q, const TLorentzVector &P_prod); // Get the coplanarity angle
      double      GetConeAngle( const TLorentzVector &P_X, const TLorentzVector &P_prod );
      vector_d    GetExclusivityVariables();
    };

    ExclusivityVariables exc_vars;
  
  public:

    Kinematics (){};
    //Kinematics( const TLorentzVector &_beam, const TLorentzVector &_targ, const TLorentzVector &_scat, const TLorentzVector &_rec, const TLorentzVector &_prod, const TLorentzVector &_spec = TLorentzVector() );
    Kinematics( const TLorentzVector &_beam, const TLorentzVector &_targ );
    void UpdateKinematics( const TLorentzVector &_scat, const TLorentzVector &_rec, const TLorentzVector &_prod, const TLorentzVector &_spec = TLorentzVector() );
    ~Kinematics(){};

    void SetKinematics();

  private:

    


};


#endif
    

