#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "KFParticle.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "kfparticle", "[kfparticle]" ) {

  //INFO("The DIS cross section for various fixed target kinematics \n"
  //    );

  GIVEN( "A single particle" ) {

    KFParticle p("part");

    std::cout << p.name << "\n";
    std::cout << p.cov_mat.size() << "\n";

    REQUIRE(p.cov_mat.size() == 0);

  }
}
