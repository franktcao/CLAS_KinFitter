#ifndef kinfit_helpers_cxx
#define kinfit_helpers_cxx

//________________________________________________________________________________________________________________________________

TLorentzVector getKFPSum( const std::vector<double> &_y, const std::vector<double> &_x, std::vector<KFParticle> &_KFParticles ){
  // Sums the relevant Lorentz vectors for the given set of KFP Particles, evaluated with new measured (y) and (x) values

  TLorentzVector P; 
  for( auto &KFP : _KFParticles ){
    P += KFP.GetP( _y, _x );
  }
  return P;
}

//________________________________________________________________________________________________________________________________

std::vector<double> getConstraints_exclusivity( const std::vector<double> &_y, const std::vector<double> &_x, int _ipart, int _ismeas, int _ivar, std::vector<KFParticle> &_KFParticles, const TLorentzVector &_P_init_0 ){
  //// Exclusivity of:
  ////   e He -> e He pi0     
  //// aka
  ////   P_beam + P_targ - ( P_e + P_he4 + P_pi0 )
  std::vector<double> result(4, 0);
  if( _ipart > 2 ) return result; 

  TLorentzVector P_init, P_fin, P_tot;

  if( _ipart < 0 ){
    // Evaluate the constraints:
    P_init = _P_init_0;
    std::vector<KFParticle> KFPs_involved;  
    KFPs_involved.assign( _KFParticles.begin(), _KFParticles.begin() + 3 );
    P_fin = getKFPSum( _y, _x, KFPs_involved );
  }
  else{
    // Returns the derivative of constraint w.r.t. the "ivar-th" variable of kfparticle ipart: P_beam + P_targ - ( P_e + P_he4 + P_pi0 )
    P_fin = _KFParticles[_ipart].GetDP( _y, _x, _ismeas, _ivar ) ;
  }
  P_tot = P_init - P_fin;
  for( int ii = 0; ii < 4; ii++ ) result.at(ii) = P_tot[ii]; 

  return result;
}

//________________________________________________________________________________________________________________________________

std::vector<double> getConstraints_decay( const std::vector<double> &_y, const std::vector<double> &_x, int _ipart, int _ismeas, int _ivar, std::vector<KFParticle> &_KFParticles ){
  //// Exclusivity of:
  ////   pi0 -> gamma1 gamma2     
  //// aka
  ////   P_pi0 - ( P_gamma1 + P_gamma_2 )
  std::vector<double> result(4, 0);
  if( 0 <= _ipart && _ipart < 2 ) return result; 

  TLorentzVector P_init, P_fin, P_tot;
  
  int ipi0 = 2;
  if( _ipart < 0 ){
    // Evaluate the constraints:
    P_init = _KFParticles[ipi0].GetP( _y, _x );
    std::vector<KFParticle> KFPs_involved;  
    KFPs_involved.assign( _KFParticles.begin() + 3, _KFParticles.begin() + 5 );
    P_fin = getKFPSum( _y, _x, KFPs_involved );
  }
  else{
    // Returns the derivative of constraint w.r.t. the "ivar-th" variable of kfparticle ipart: P_pi0 - ( P_gamma1 + P_gamma2 )
    if( _ipart == ipi0 ) P_init = _KFParticles[_ipart].GetDP( _y, _x, _ismeas, _ivar ) ;
    else                P_fin  = _KFParticles[_ipart].GetDP( _y, _x, _ismeas, _ivar ) ;
  }
  P_tot = P_init - P_fin;
  for( int ii = 0; ii < 4; ii++ ) result.at(ii) = P_tot[ii]; 

  return result;
}

//________________________________________________________________________________________________________________________________

#endif



