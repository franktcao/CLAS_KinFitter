#include "KFParticle.h"
#include <string>

// Useful shortcuts
using namespace std;
using vector_d  = vector<double>;
using vector_d2 = vector<vector<double>>;
using vector_s  = vector<string>;

//________________________________________________________________________________________________________________________________

KFParticle::KFParticle( const string &_name, int _isMeas, const TLorentzVector &_P, const TVector3 &_V, const vector_d &_widths )
    : name(_name), P(_P), V(_V) {

      vector_d y, x;
      
      if( _isMeas <= 0 ) SetParticleToUnmeasured(); 
      else               SetParticleDefaults();

      std::tie( y, x ) = GetKFVars( _P, _V );

      n_meas   = y.size(); 
      n_unmeas = x.size(); 

      SetSigNames( );
      if( _widths.size() == 0 ){
        vector_d widths( n_meas );
        SetCovMat( widths, true );
      }
      else SetCovMat( _widths, true );
}

//________________________________________________________________________________________________________________________________

void KFParticle::SetParticleDefaults(){
  // Returns a std::tuple of the measured and unmeasured kin. fit var's
  auto kf2physvars 
    = []( const TLorentzVector & _P, const TVector3 &_V ){ 
      vector_d kfvars_meas{ _P.P(), _P.Theta(), _P.Phi() };
      vector_d kfvars_unmeas;
      return std::make_tuple( kfvars_meas, kfvars_unmeas ); 
    };
  auto phys2kfvars
    = [&]( const vector_d &_meas, const vector_d &_unmeas, const TLorentzVector &_P){ 
      TLorentzVector P;
      TVector3 p;
      p.SetMagThetaPhi( _meas.at(0), _meas.at(1), _meas.at(2) );
      P.SetVectM( p, _P.M() );

      TVector3 V;
      return std::make_tuple( P, V ); 
    };
  std::vector<std::string> n_meas, n_unmeas;
  std::vector<std::string> u_meas, u_unmeas;
  n_meas= {"p", "#theta", "#phi"};
  u_meas= {"[GeV/c]", "[rad.]", "[rad.]"};
        
  std::vector<std::pair<double,double>> mm_meas, mm_unmeas;
  mm_meas.push_back( {0.2, 6} );
  mm_meas.push_back( {0., TMath::Pi()/2} );
  mm_meas.push_back( {-TMath::Pi(), TMath::Pi()} );
  
  SetProperties( kf2physvars, phys2kfvars, n_meas, u_meas, mm_meas, n_unmeas, u_unmeas, mm_unmeas );

}

//________________________________________________________________________________________________________________________________

void KFParticle::SetParticleToUnmeasured(){
 auto kf2physvars 
    = []( const TLorentzVector & _P, const TVector3 &_V ){ 
      vector_d kfvars_meas;
      vector_d kfvars_unmeas{ _P.P(), _P.Theta(), _P.Phi() }; 
      return std::make_tuple( kfvars_meas, kfvars_unmeas ); 
    };
  auto phys2kfvars
    = []( const vector_d &_meas, const vector_d &_unmeas, const TLorentzVector &_P){ 
      TLorentzVector P;
      TVector3 p;
      p.SetMagThetaPhi( _unmeas.at(0), _unmeas.at(1), _unmeas.at(2) );
      P.SetVectM( p, _P.M() );

      TVector3 V;
      return std::make_tuple( P, V ); 
    };
  std::vector<std::string> n_meas, n_unmeas;
  std::vector<std::string> u_meas, u_unmeas;
  n_unmeas= {"p", "#theta", "#phi"};
  u_unmeas= {"[GeV/c]", "[rad.]", "[rad.]"};
        
  std::vector<std::pair<double,double>> mm_meas, mm_unmeas;
  mm_unmeas.push_back( {0.2, 6} );
  mm_unmeas.push_back( {0., TMath::Pi()/2} );
  mm_unmeas.push_back( {-TMath::Pi(), TMath::Pi()} );
  
  SetProperties( kf2physvars, phys2kfvars, n_meas, u_meas, mm_meas, n_unmeas, u_unmeas, mm_unmeas );
}

//________________________________________________________________________________________________________________________________

vector_d2 KFParticle::Get2DCovMat( ){
  vector_d2 result( n_meas, vector_d( n_meas, 0 ) );
  if( isCovMatDiag ){
    for( int ii = 0; ii < n_meas; ii++ ) result.at(ii).at(ii) = cov_mat.at(ii); 
  }
  else{
    for( int irow = 0; irow < n_meas; irow++ ){
      // nlast is the total number of elements above this row of pascal's triangle
      int nlast = ( irow * (irow + 1) )/2; 
      for( int icol = 0; icol < irow+1; icol++){
        int icovmat = nlast + icol;
        result.at(irow).at(icol) = result.at(icol).at(irow) = cov_mat.at(icovmat); 
      }
    }
  }
  return result;
}

//________________________________________________________________________________________________________________________________

void KFParticle::SetProperties( 
    const std::function< std::tuple<std::vector<double>, std::vector<double> >( const TLorentzVector&,      const TVector3& ) >                                   &_kf2physvars,
    const std::function< std::tuple<TLorentzVector,      TVector3            >( const std::vector<double>&, const std::vector<double>&, const TLorentzVector& ) > &_phys2kfvars,
    const std::vector<std::string>              &_names_meas,
    const std::vector<std::string>              &_units_meas,
    const std::vector<std::pair<double,double>> &_minmax_meas,
    const std::vector<std::string>              &_names_unmeas,
    const std::vector<std::string>              &_units_unmeas,
    const std::vector<std::pair<double,double>> &_minmax_unmeas
){
  GetKFVars     = _kf2physvars;
  GetPhysVars   = _phys2kfvars;
  names_meas    = _names_meas;
  units_meas    = _units_meas;
  minmax_meas   = _minmax_meas;
  names_unmeas  = _names_unmeas;
  units_unmeas  = _units_unmeas;
  minmax_unmeas = _minmax_unmeas;
}

//________________________________________________________________________________________________________________________________

void KFParticle::SetCovMat( const vector_d &_cov_mat, bool _isDiag  ){
  // Redundant but may be needed if there's another way to initialize the cov. matrix
  // where _isDiag might be useful
  SetCovMat_triag( _cov_mat );
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetCovMat_triag( const vector_d &_cov_mat ){
  // When you have one half of the symmetric matrix
  isCovMatDiag = false;
  cov_mat = _cov_mat;
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetCovMat_diag( const vector_d &_sigma2s ){
  // When you don't have a full covariance matrix but an estimate, using sigmas
  isCovMatDiag = true;
  cov_mat = _sigma2s;
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetCovMat_sigmas( const vector_d &_sigmas ){
  // When you don't have a full covariance matrix but an estimate, using sigmas
  vector_d sigma2s = _sigmas;
  for( auto &s : sigma2s ) s *= s;
  SetCovMat_diag( sigma2s );
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetPAndCovMat( const TLorentzVector &_P, const vector_d &_cov_mat_triag ){
 SetP( _P );
 SetCovMat( _cov_mat_triag );
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetPAndCovMat_diag( const TLorentzVector &_P, const vector_d &_cov_mat_diag ){
 SetP( _P );
 SetCovMat_diag( _cov_mat_diag );
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetPAndSigmas( const TLorentzVector &_P, const vector_d &_sigmas ){
 SetP( _P );
 SetCovMat_sigmas( _sigmas );
}

////________________________________________________________________________________________________________________________________

void KFParticle::SetPVAndCovMat( const TLorentzVector &_P, const TVector3 &_V, const vector_d &_cov_mat_triag ){
 SetP( _P );
 SetV( _V );
 SetCovMat( _cov_mat_triag );
}

//////________________________________________________________________________________________________________________________________

TLorentzVector KFParticle::GetP(const vector_d &_meas, const vector_d &_unmeas ){ 
  // Gets all measured and unmeasured variables of kin. fit and takes only relevant part to construct 4 momentum

  vector_d meas;
  meas.assign( _meas.begin() + ipart_y, _meas.begin() + ipart_y + n_meas );
  vector_d unmeas;
  unmeas.assign( _unmeas.begin() + ipart_x, _unmeas.begin() + ipart_x + n_unmeas );

  return get<TLorentzVector>( GetPhysVars( meas, unmeas, P ) ); 
}
////________________________________________________________________________________________________________________________________

TVector3       KFParticle::GetV(const vector_d &_meas, const vector_d &_unmeas ){ 
  
  vector_d meas;
  meas.assign( _meas.begin() + ipart_y, _meas.begin() + ipart_y + n_meas );
  vector_d unmeas;
  unmeas.assign( _unmeas.begin() + ipart_x, _unmeas.begin() + ipart_x + n_unmeas );

  return get<TVector3>(       GetPhysVars( meas, unmeas, P ) ); 
}
////________________________________________________________________________________________________________________________________

// One-liners
void           KFParticle::SetName        ( const string &n          ){ name = n; }
void           KFParticle::SetP           ( const TLorentzVector &_P ){ P = _P; }
void           KFParticle::SetV           ( const TVector3 &_V       ){ V = _V; }
void           KFParticle::SetSigNames    (                          ){ for( auto &kfname : names_meas ) kfname += "_{" + name + "}"; for( auto &kfname : names_unmeas ) kfname += "_{" + name + "}"; }
void           KFParticle::SetMinMaxMeas  ( const vector_p &minmax   ){ minmax_meas   = minmax; }
void           KFParticle::SetMinMaxUnmeas( const vector_p &minmax   ){ minmax_unmeas = minmax; }

TLorentzVector KFParticle::GetP          ( ){ return P;                   }
TVector3       KFParticle::GetV          ( ){ return V;                   }
double         KFParticle::GetM          ( ){ return GetP().M();          }
double         KFParticle::GetVz         ( ){ return GetV().Z();          }
int            KFParticle::GetNKFVars    ( ){ return n_meas + n_unmeas;   }           
int            KFParticle::GetIndexY0    ( ){ return ipart_y;             }           
int            KFParticle::GetIndexX0    ( ){ return ipart_x;             }           
int            KFParticle::GetNMeasured  ( ){ return n_meas;              }           
int            KFParticle::GetNUnmeasured( ){ return n_unmeas;            }           
vector_d       KFParticle::GetMeasured   ( ){ return std::get<0>(GetKFVars( P, V )); }
vector_d       KFParticle::GetUnmeasured ( ){ return std::get<1>(GetKFVars( P, V )); }


////________________________________________________________________________________________________________________________________



////________________________________________________________________________________________________________________________________
//
//std::tuple<TLorentzVector, TVector3> KFParticle::GetPhysVarsDers( vector_d &_meas, vector_d &_unmeas, int _ismeas, int _ivar ){
//
//  TLorentzVector P_hi, P_lo;
//  TVector3 V_hi, V_lo;
//  double h = 1E-5;
//  if( _ismeas == 1 ){
//    vector_d &kfvars = _meas;
//
//
//    h = 0.01 * kfvars.at(_ivar);
//    kfvars.at(_ivar) += h;
//    std::tie( P_hi, V_hi ) = GetPhysVars( _meas, _unmeas, P );
//
//    kfvars.at(_ivar) -= 2*h; // Undoing the addition as well as setting the low
//    std::tie( P_lo, V_lo ) = GetPhysVars( _meas, _unmeas, P );
//    kfvars.at(_ivar) += h;
//  }
//  else{
//    vector_d &kfvars = _unmeas;
//
//    h = 0.01 * kfvars.at(_ivar);
//    kfvars.at(_ivar) += h;
//    std::tie( P_hi, V_hi ) = GetPhysVars( _meas, _unmeas, P );
//
//    kfvars.at(_ivar) -= 2*h; // Undoing the addition as well as setting the low
//    std::tie( P_lo, V_lo ) = GetPhysVars( _meas, _unmeas, P );
//    kfvars.at(_ivar) += h;
//  }
//
//  TLorentzVector dP = P_hi - P_lo;
//  dP *= 1/(2*h);
//  TVector3 dV = V_hi - V_lo;
//  dV *= 1/(2*h);
//
//  return std::make_tuple( dP, dV );
//};
//
//////________________________________________________________________________________________________________________________________
//
//TLorentzVector KFParticle::GetDP( const vector_d &_meas, const vector_d &_unmeas, int _ismeas, int _ivar ){ 
//  vector_d meas;
//  meas.assign( _meas.begin() + ipart_y, _meas.begin() + ipart_y + n_meas );
//  vector_d unmeas;
//  unmeas.assign( _unmeas.begin() + ipart_x, _unmeas.begin() + ipart_x + n_unmeas );
//
//  return get<TLorentzVector>( GetPhysVarsDers( meas, unmeas, _ismeas, _ivar ) ); 
//}
//
//////________________________________________________________________________________________________________________________________
//
//TVector3 KFParticle::GetDV( const vector_d &_meas, const vector_d &_unmeas, int _ismeas, int _ivar ){ 
//  vector_d meas;
//  meas.assign( _meas.begin() + ipart_y, _meas.begin() + ipart_y + n_meas );
//  vector_d unmeas;
//  unmeas.assign( _unmeas.begin() + ipart_x, _unmeas.begin() + ipart_x + n_unmeas );
//
//  return get<TVector3>( GetPhysVarsDers( meas, unmeas, _ismeas, _ivar) ); 
//}
