#include "Kinematics.h"

////________________________________________________________________________________________________________________________________

Kinematics::Kinematics( const TLorentzVector &_beam, const TLorentzVector &_targ ){
  P_beam = _beam;
  P_targ = _targ;
}

////________________________________________________________________________________________________________________________________

void Kinematics::UpdateKinematics( const TLorentzVector &_scat, const TLorentzVector &_rec, const TLorentzVector &_prod, const TLorentzVector &_spec ){
  P_scat = _scat;
  P_rec  = _rec;
  P_prod = _prod;
  P_spec = _spec;
  P_q = P_beam - P_scat;
  SetKinematics();
  exc_vars.SetExclusivityVariables(P_beam, P_targ, P_scat, P_rec, P_prod, P_spec);
}

////________________________________________________________________________________________________________________________________

void Kinematics::SetKinematics(){
  double E_beam  = P_beam.E();
  double theta_e = P_scat.Theta(); 
  double nu      = P_q.E();
  double M_p     = 0.938272;

  Q2 = 4 * E_beam * P_scat.P() * pow( TMath::Sin(theta_e/2) , 2 );
  x  = Q2/2/M_p/nu;
  t = (P_targ - P_rec).M2();

  TVector3 LeptonicPlane = (P_beam.Vect()).Cross(P_scat.Vect());
  TVector3 HadronicPlane = (P_rec.Vect()).Cross(P_q.Vect());

  phi = LeptonicPlane.Angle(HadronicPlane)* TMath::RadToDeg();
  if(LeptonicPlane.Dot(P_rec.Vect())>0.)  phi = 360.0 - phi;
}

////________________________________________________________________________________________________________________________________

void Kinematics::ExclusivityVariables::SetExclusivityVariables( const TLorentzVector &P_beam, const TLorentzVector &P_targ, const TLorentzVector &P_scat, 
            const TLorentzVector &P_rec, const TLorentzVector &P_prod, const TLorentzVector &P_spec ){
  // Give the Momentum 4-Vector for the:
  //    - Recoiled particle ( {rec} He4 or proton // coherent or incoherent ) ,
  //    - And Produced Particle ( {prod} pi0 or eta or dvcs photon )
  // and it returns the relevant values in cartridge with
  //    0. Missing Mass of final state : e + prod 
  //    1. Missing Mass of final state : e + rec 
  //    2. Missing Mass of final state : e + rec + prod
  //    3. Missing P_x of final state : e + rec + prod
  //    4. Missing P_y of final state : e + rec + prod
  //    5. Missing P_t of final state : e + rec + prod
  //    6. Missing Energy of final state : e + rec + prod
  //    7. Cone Angle {theta} of final state : e + rec + prod
  //    8. Coplanarity {dPhi} of final state : e + rec + prod
  // for each component of cartridge, respectively
  
  P_init = P_beam + P_targ;
  P_q = P_beam - P_scat;
  
  // Final State: e + produced particle
  TLorentzVector P_fin = P_scat + P_prod + P_spec;
  TLorentzVector P_X = P_init - P_fin;
  M2_X_rec = P_X.M2(); 

  // Final State: e + recoil particle
  P_fin = P_scat + P_rec + P_spec;
  P_X = P_init - P_fin;
  M2_X_prod = P_X.M2(); 
  theta_X_rec = GetConeAngle( P_X, P_prod );


  // Final State: e + recet particle + produced particle
  P_fin = P_scat + P_rec + P_prod + P_spec;
  P_X = P_init - P_fin;
  M2_X = P_X.M2(); 
  px_X = P_X.Px();
  py_X = P_X.Py();
  pt_X = P_X.Pt();
  E_X  = P_X.E();
  
  // dPhi 
  dphi_rec_prod = GetCoplanarity( P_rec, P_q, P_prod );
}

////________________________________________________________________________________________________________________________________

vector_d Kinematics::ExclusivityVariables::GetExclusivityVariables(){
  return vector_d{ M2_X_rec , M2_X_prod, M2_X, px_X, py_X, pt_X, E_X, theta_X_rec, dphi_rec_prod };
}

////________________________________________________________________________________________________________________________________

double Kinematics::ExclusivityVariables::GetConeAngle( const TLorentzVector &P_X, const TLorentzVector &P_prod) {
  // Give the Momentum 4-Vector for the:
  //    Missing Particle,
  //    And Produced Particle ( pi0 or eta or dvcs photon )
  // and it returns cone angle, theta (angle between the two vectors)

  double theta = ((P_prod.Vect()).Angle(P_X.Vect())) * TMath::RadToDeg();
  return theta;
}

////________________________________________________________________________________________________________________________________

double Kinematics::ExclusivityVariables::GetCoplanarity( const TLorentzVector &P_rec, const TLorentzVector &P_q, const TLorentzVector &P_prod) {
  // Give the Momentum 4-Vector for the:
  //    Target (He4 or P),
  //    Virtual Photon,
  //    And Produced Particle ( pi0 or eta or dvcs photon )
  // and it returns coplanarity, dPhi (angle between the two planes ( Plane{recet, virtual photon} and Plane{ recet, produced particle }

  //TVector3 plane1 = (P_rec.Vect()).Cross(P_q.Vect());
  //TVector3 plane2 = (P_q.Vect()).Cross(P_prod.Vect());
  TVector3 plane1 = (P_rec.Vect()).Cross(P_q.Vect());
  TVector3 plane2 = (P_rec.Vect()).Cross(P_prod.Vect());
  Double_t dPhi = TMath::RadToDeg() * plane1.Angle(plane2);
  //dPhi = float(180./TMath::Pi()) * plane1.Angle(plane2);

  if( (plane1.Cross(plane2)).Z() < 0 ) dPhi = -dPhi; 

  return dPhi;
}

////________________________________________________________________________________________________________________________________



