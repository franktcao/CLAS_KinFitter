#include "KinFitter.h"
#include <Eigen/Dense>
#include "dollar/dollar.hpp"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Constructor with extra constraints (other than energy and momentum conservation... like an invariant mass)
KinFitter::KinFitter( vector_kfp &_KFParticles, const std::function<vector_d(const vector_d&, const vector_d&)>  &constraints ){
  
  set_of_constraints.push_back( constraints );
  InitParticles( _KFParticles );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Constructor with extra constraints (other than energy and momentum conservation... like an invariant mass)
KinFitter::KinFitter( vector_kfp &_KFParticles, const vector_func &_set_of_constraints ){
  
  set_of_constraints = _set_of_constraints;
  InitParticles( _KFParticles );
  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::InitParticles( vector_kfp &_KFParticles ){
  
  for( auto &p: _KFParticles ){ AddParticle( p ); p.ipart_KFP = &p - &_KFParticles[0]; }
  
  int n_meas   = GetNMeasured();
  int n_unmeas = GetNUnmeasured();
  
  // Copy after setting up so that it can know what ipart_yx is 
  KFParticlesIn   = _KFParticles;
  epsilon         = vector_d( n_meas, 0 );
  sigma2s_epsilon = vector_d( n_meas, 0 );
  
  n_constraints = 0;
  for( auto &cc : set_of_constraints ) n_constraints += cc( eta, x0 ).size(); 
  
  ndf = n_constraints - n_unmeas; 
  if( ndf < 0 ){ cout << "Number of Degrees of Freedom < 0! : " << ndf << endl; }
  else{ isValid = true; }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::UpdateKFParticles( vector_kfp &_KFPsIn ){
  
  KFParticlesIn = _KFPsIn;

  cov_mat_compact.clear();
  C_eta.clear();

  for( auto &KFP : _KFPsIn ){
    TLorentzVector P = KFP.GetP();
    TVector3       V = KFP.GetV();

    vector_d meas, unmeas;
    std::tie( meas, unmeas ) = KFP.GetKFVars( P, V );

    std::copy( meas.begin(),   meas.end(),   eta.begin() + KFP.ipart_y );
    std::copy( unmeas.begin(), unmeas.end(), x0.begin()  + KFP.ipart_x );
    
    isCovMatDiag = true;
    if( KFP.GetNMeasured() > 0 ){
      vector_d cov_mat = KFP.cov_mat; 

      bool isCovMatDiag_part = KFP.isCovMatDiag;
      if( isCovMatDiag_part == false ) isCovMatDiag = false;
      ExtendCovMat( cov_mat, isCovMatDiag_part );
    }
  }

  epsilon         .assign( eta.size(), 0 );
  sigma2s_epsilon .assign( eta.size(), 0 );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::SetFittedKFParticles( const vector_d &y, const vector_d &x ){
  KFParticlesOut   = KFParticlesIn;

  KFParticle KFP;
  TLorentzVector P4;
  TVector3 V;
  for( auto &KFP : KFParticlesOut ){
    
    int n_meas     = KFP.GetNMeasured();
    int n_unmeas   = KFP.GetNUnmeasured();
    int ipart_y    = KFP.GetIndexY0();
    int ipart_x    = KFP.GetIndexX0();

    vector_d meas  (n_meas  );
    vector_d unmeas(n_unmeas);
    meas  .assign( y.begin() + ipart_y + 0, y.begin() + ipart_y + n_meas   );
    unmeas.assign( x.begin() + ipart_x + 0, x.begin() + ipart_x + n_unmeas );
    
    std::tie( P4, V ) = KFP.GetPhysVars( meas, unmeas, KFP.GetP() );
    
    KFP.SetP(P4);
    KFP.SetV(V);
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::AddParticle( KFParticle &KFP ){
  
  TLorentzVector P = KFP.GetP();
  TVector3       V = KFP.GetV();

  vector_d meas, unmeas ;
  std::tie( meas, unmeas ) = KFP.GetKFVars( P, V );
  
  cov_mat_compact.clear();
  isCovMatDiag = true;
  if( meas.size() > 0 ){
    KFP.ipart_y = eta.size();
    vector_d cov_mat = KFP.cov_mat; 
    bool isCovMatDiag_part = KFP.isCovMatDiag;
    if( isCovMatDiag_part == false ) isCovMatDiag = false;
    ExtendCovMat( cov_mat, isCovMatDiag_part );
  }
  if( unmeas.size() > 0 ) KFP.ipart_x = x0.size(); 

  
  eta.insert( eta.end(), meas.begin(),   meas.end() );
  x0 .insert( x0.end(),  unmeas.begin(), unmeas.end() );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::ExtendCovMat( vector_d &_cov_mat, bool _isCovMatDiag ){
  // Extend the covariance C_eta with the input _cov_mat 

  if( _isCovMatDiag ) cov_mat_compact.insert( cov_mat_compact.end(), _cov_mat.begin(), _cov_mat.end() );
  else{
    //// C_eta as a 2D std::vector 
    // Pad existing C_eta with zeros of particle's cov_mat
    int nrow =  (1 + sqrt( 1 + 8 * _cov_mat.size() ) )/2 - 1;
    vector_d zeros( nrow, 0 );
    for( auto &row : C_eta ){ row.insert( row.end(), zeros.begin(), zeros.end() ); }

    //// Add columns with padding
    // Note: This only stores the bottom left triangle 
    int nrows_old = C_eta.size();
    for( int irow = 0; irow < nrow; irow++ ){
      //// nlast is the total number of elements above this row of pascal's triangle
      vector_d newrow( nrows_old + nrow, 0 );
      int nlast = ( irow * (irow + 1) )/2; 
      int ncol  = irow + 1;
      for( int icol = 0; icol < ncol; icol++) newrow.at(nrows_old + icol) = _cov_mat.at(nlast + icol);
      C_eta.push_back( newrow ); 
    }
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::ProcessFit( int nter, double cl_thresh ){
  if( !isValid ) return;
  
  vector_d delta, xi, sigma2s_temp;
  
  iter = 0;
  chi2 = 0;
  n_chi2_increases = 0;
  vector_d y = eta;
  vector_d x = x0;
  double L_old = 0;
  double L_new = 0;
  while( iter < nter ){
    std::tie( delta, xi, sigma2s_temp, L_new ) = GetNewDeltaXiAndErrors( y, x );
		if( isReiterate(delta, cl_thresh) ){
    //if( L_new < L_old || iter == 0 ){
      L_old = L_new;
			//isReiterate(delta);
      sigma2s_epsilon = sigma2s_temp;
      for( unsigned ii = 0; ii < epsilon.size(); ii++ ){ epsilon[ii] += delta[ii]; y[ii] += delta[ii]; }
      for( unsigned ii = 0; ii < x.size();       ii++ )  x[ii]       += xi[ii];                        
      iter++;
    }
    else break; 
  }
  if( iter == nter ) exit_status = -3;
  SetFittedKFParticles( y, x );
  StageOutputs(); 
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
std::tuple< vector_d, vector_d, vector_d, double  > KinFitter::GetNewDeltaXiAndErrors( vector_d &y, vector_d &x ){
  // Gets new increments (delta and xi) for measured and unmeasured variables (y and x), respectively
  // Puts them into a matrix where the first column is delta and the second is xi
  
  //// Constraint Vector and Matrices -------------------------------------------------------------------------------------
  //// Create new c, B, and A
  
  int n_meas   = GetNMeasured();
  int n_unmeas = GetNUnmeasured();
  
  using namespace Eigen;
  VectorXd c( n_constraints ); 
  MatrixXd B( n_constraints, n_meas ); 
  MatrixXd A;
  
  if( n_unmeas > 0 ) A = MatrixXd( n_constraints, n_unmeas      ); 
  else               A = MatrixXd( n_constraints, n_constraints ); // This size for multiplication reasons
  A.setZero(); 
  
  // Construct c from constraints
  auto eval_constraints = [&]( const vector_d &_y, const vector_d &_x ){ 
    vector_d result;
    for( auto &cc : set_of_constraints ){ 
      vector_d constraints = cc( _y, _x );
      result.insert( result.end(), constraints.begin(), constraints.end() );
    }
    return result;
  };
  auto eval_derivatives1 = [&]( int _ismeas, int _ivar ){ 
    int idx = _ivar;
    if( _ismeas < 1 ) idx += y.size();
    vector_d kfvars = y;
    kfvars.insert( kfvars.end(), x.begin(), x.end() );

    double h = std::abs(1E-2 * kfvars.at(idx));

    vector_d meas, unmeas, c_hi, c_lo;
    
    kfvars.at( idx ) += h;
    meas  .assign( kfvars.begin(),             kfvars.begin() + y.size()            );
    unmeas.assign( kfvars.begin() + y.size(),  kfvars.begin() + y.size() + x.size() );
    c_hi = eval_constraints( meas, unmeas ); 
    
    kfvars.at( idx ) -= 2*h;
    meas  .assign( kfvars.begin(),             kfvars.begin() + y.size()            );
    unmeas.assign( kfvars.begin() + y.size(),  kfvars.begin() + y.size() + x.size() );
    c_lo = eval_constraints( meas, unmeas ); 
    
    VectorXd der = Map<VectorXd>( c_hi.data(), c_hi.size() ) - Map<VectorXd>( c_lo.data(), c_lo.size() );
    der *= 1/(2*h);
    vector_d result( der.data(), der.data() + c_hi.size() );

    return result;
  };
  auto eval_derivatives2 = [&]( int _ismeas, int _ivar ){ 
    double h;
    vector_d c_hi, c_lo;
    
    double dh = 1E-2;
    if( _ismeas < 1 ){
      h = dh * (x.at( _ivar));
      
      x.at( _ivar ) *= (1+dh);
      c_hi = eval_constraints( y, x );
      x.at( _ivar ) /= (1+dh);
      
      x.at( _ivar ) *= (1-dh);
      c_lo = eval_constraints( y, x );
      x.at( _ivar ) /= (1-dh);
    }
    else{
      h = dh * (y.at( _ivar));
      
      y.at( _ivar ) *= (1+dh);
      c_hi = eval_constraints( y, x );
      y.at( _ivar ) /= (1+dh);
      
      y.at( _ivar ) *= (1-dh);
      c_lo = eval_constraints( y, x );
      y.at( _ivar ) /= (1-dh);
    }

    VectorXd der = Map<VectorXd>( c_hi.data(), c_hi.size() ) - Map<VectorXd>( c_lo.data(), c_lo.size() );
    der *= 1/(2*h);
    vector_d result( der.data(), der.data() + c_hi.size() );

    return result;
  };

  vector_d c_std = eval_constraints( y, x );  
  c = Map<VectorXd>( c_std.data(), n_constraints );

  for( int jj = 0; jj < n_meas;   jj++ ) B.col(jj)  = Map<VectorXd>( eval_derivatives2( 1, jj ).data(), n_constraints ); 
  for( int jj = 0; jj < n_unmeas; jj++ ) A.col(jj)  = Map<VectorXd>( eval_derivatives2( 0, jj ).data(), n_constraints ); 
  //cout << B << endl;

  //// Constraint Vector and Matrices XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  
  using MatrixXdRM = Matrix<double, Dynamic, Dynamic, RowMajor>;
  vector_d C_eta_std;
  
  MatrixXd C_eta_eig;
  if( isCovMatDiag ){
    Map<VectorXd> C_eta_diag( cov_mat_compact.data(), n_meas );
    C_eta_eig = C_eta_diag.asDiagonal();
  }
  else{
    vector_d flat_cov_mat = GetFlattenedCovMat();
    Map<MatrixXd> C_eta_triag( flat_cov_mat.data(), n_meas, n_meas );
    C_eta_eig.triangularView<Upper>() = C_eta_eig.transpose().eval().triangularView<Upper>();
    C_eta_eig = C_eta_triag;
  }
  
  // Turn the std::vectors to Eigen objects that can be manipulated with linear algebra
  VectorXd epsilon_eig = Map<VectorXd>( epsilon.data(), n_meas );
  
  ////// Get vectors and matrices needed for calculation
  //// Get r
  VectorXd r(c);
  r -= B * epsilon_eig;

  //////// Get C_B
  MatrixXd C_B = B * C_eta_eig * B.transpose();
  C_B = C_B.inverse();
  MatrixXd C_x;
  C_x = A.transpose() * C_B * A;
  if( n_unmeas > 0 ) C_x = C_x.inverse(); 
  
  //// Get xi
  ////// xi = -(C_x A^T C_B) r
  VectorXd xi = -(C_x * A.transpose() * C_B) * r;

  ////// Get mu
  //////// mu = C_B( (A xi) + r )
  VectorXd mu(r);
  mu += A*xi; 
  mu  = C_B*mu;
  
  VectorXd delta_eig(epsilon_eig);
  delta_eig += (C_eta_eig * B.transpose()) * mu; 
  delta_eig *= -1;
  

  
  // Get C_eps, the new covariance matrix
  MatrixXd G = B.transpose() * C_B * B;
  MatrixXd H = B.transpose() * C_B * A;
  MatrixXd C_eps  = C_eta_eig * G * C_eta_eig;
  C_eps -= C_eta_eig * (H * C_x * H.transpose()) * C_eta_eig;
  
  VectorXd sigma2s_eps = C_eps.diagonal();

  // eig->std
  vector_d delta_std( delta_eig.data(), delta_eig.data() + n_meas   );
  vector_d xi_std(    xi.data(),    xi.data()    + n_unmeas );
  vector_d sigma2s_eps_std( sigma2s_eps.data(), sigma2s_eps.data() + n_meas );
  
  //// The complementary part to the chi2 term, the constraints evaluated at the new values about the old values
  //VectorXd f = A*xi + B*delta_eig + c; 
  //double chi2_new = CalcChi2( delta_std ); 
  //double cstr = mu.transpose() * f;
  //// The Lagrangian evaluated
  ////double L = chi2_new + 2 * (cstr);
  double L = 0; 
  
  return std::make_tuple( delta_std, xi_std, sigma2s_eps_std, L );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double KinFitter::CalcChi2( vector_d &delta ){
  using namespace Eigen;
  using MatrixXdRM = Matrix<double, Dynamic, Dynamic, RowMajor>;

  int n_meas = delta.size();

  MatrixXd C_eta_eig;
  if( isCovMatDiag ){
    Map<VectorXd> C_eta_diag( cov_mat_compact.data(), n_meas );
    C_eta_eig = C_eta_diag.asDiagonal();
  }
  else{
    vector_d flat_cov_mat = GetFlattenedCovMat();
    Map<MatrixXd> C_eta_triag( flat_cov_mat.data(), n_meas, n_meas );
    C_eta_eig.triangularView<Upper>() = C_eta_eig.transpose().eval().triangularView<Upper>();
    C_eta_eig = C_eta_triag;
  }

  VectorXd epsilon_eig = Map<VectorXd>( epsilon.data(), n_meas );
  VectorXd delta_eig   = Map<VectorXd>( delta.data(),   n_meas );
  VectorXd epsilon_new = epsilon_eig + delta_eig;
  double chi2_new = (epsilon_new.transpose()) * C_eta_eig.inverse() * epsilon_new; 
  //if( isnan(chi2_new) ) cout << "EPSIJFIDJF " << delta_eig << " fsjfisdfjd " << epsilon_eig << endl;
  
  return chi2_new;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void KinFitter::SetPulls( ){
  // Returns a vector of pull distributions
  // Get pulls z_i ( = [ eta_i - y_i ] / sqrt( sigma^4_eta_i - sigma^2_y_i )
  using namespace Eigen;

  int n_meas = GetNMeasured();

  // std->eig
  VectorXd num   = Map<VectorXd>( epsilon.data(),         n_meas );
  VectorXd denom = Map<VectorXd>( sigma2s_epsilon.data(), n_meas );
  
  std::vector<int> flags;
  for( int ii = 0; ii < n_meas; ii++ ){ 
    if( denom(ii) <= 0 ) flags.push_back(ii); 
  }

  VectorXd pulls_eig( n_meas );
  if( flags.size() == 0 ){
    pulls_eig =  num.cwiseProduct(  (denom.cwiseSqrt()).cwiseInverse() ) ;    // Element-wise division!
  }
  else{
    cout << endl;
    cout << "======================================== " << endl;
    cout << "======================================== " << endl;
    cout << "\tepsilon " << endl;
    for( auto val : epsilon ) cout << val << " "; 
    cout << endl;
    cout << "======================================== " << endl;
    
    cout << "eta " << endl;
    for( auto val : eta ) cout << val << " "; 
    cout << endl;

    cout << "x0 " << endl;
    for( auto val : x0 ) cout << val << " "; 
    cout << endl;

    cout << "Pull " << endl;
    for( auto iden : flags ){
      cout << iden << " " << denom(iden) << endl;
      pulls_eig(iden)= -4; 
    }
    cout << "CL " << confLevel << endl;
    cout << "======================================== " << endl;
    cout << "======================================== " << endl;
  }
  // eig->std
  pulls = vector_d( pulls_eig.data(), pulls_eig.data() + n_meas );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool KinFitter::isReiterate( vector_d &delta, double cl_thresh ){
 double chi2_new = CalcChi2( delta ); 
 if( iter == 0 ){ chi2 = chi2_new;  return true; }

 //if( n_chi2_increases > 1                ){ exit_status = -2;  return false; }  
 if( TMath::Prob( chi2_new, ndf ) < cl_thresh ){ exit_status = -1;  return false; } 
 if( abs(chi2_new-chi2)/chi2      < 1E-5      ){ exit_status =  1;  return false; }  

 if( chi2_new > chi2  ) n_chi2_increases++;
 else                   n_chi2_increases = 0;
 
 chi2 = chi2_new; 
 return true; 
 
 //  //if( abs(chi2_new
   //  //else{ chi2 = chi2_new; return true; }
   //  return false; 
   //}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vector_d KinFitter::GetMeasured( int ifit ){
  vector_kfp kfps;
  vector_d meas;
  if     ( ifit == 0 ) kfps = KFParticlesIn;
  else if( ifit == 1 ) kfps = KFParticlesOut;
  for( auto &kfp : kfps ){
    vector_d meas_i = kfp.GetMeasured();
    meas.insert( meas.end(), meas_i.begin(), meas_i.end() );
  }
  return meas;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vector_d KinFitter::GetUnmeasured( int ifit ){
  vector_kfp kfps;
  vector_d unmeas;
  if     ( ifit == 0 ) kfps = KFParticlesIn;
  else if( ifit == 1 ) kfps = KFParticlesOut;
  for( auto &kfp : kfps ){
    vector_d unmeas_i = kfp.GetUnmeasured();
    unmeas.insert( unmeas.end(), unmeas_i.begin(), unmeas_i.end() );
  }
  return unmeas;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KinFitter::StageOutputs( ){
  SetConfLevel( );
  SetPulls();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vector_d KinFitter::GetFlattenedCovMat( ){ 
  vector_d result;
  for( auto &row : C_eta ) result.insert( result.end(), row.begin(), row.end() );
  return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// One-Liners
void KinFitter::SetConfLevel  ( ){ confLevel = TMath::Prob( chi2, ndf ); }
int  KinFitter::GetNPart      ( ){ return KFParticlesIn.size(); }
int  KinFitter::GetNMeasured  ( ){ return eta.size();           }
int  KinFitter::GetNUnmeasured( ){ return x0.size();            }

void KinFitter::SetCovarianceMatrix( const vector_d &_cov_mat, bool _isDiag ){ isCovMatDiag = _isDiag, cov_mat_compact = _cov_mat; }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



