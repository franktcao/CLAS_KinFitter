cmake_minimum_required (VERSION 3.1)

add_library(FCKinFitter SHARED 
  src/KinFitter.cxx
  )


target_include_directories(FCKinFitter
    PUBLIC 
        $<INSTALL_INTERFACE:include>    
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        PUBLIC ${EIGEN3_INCLUDE_DIRS}
)
target_compile_features(FCKinFitter PUBLIC cxx_std_17)

#target_link_libraries(KinFitter
#  PUBLIC InSANEbase
#)

#file(GLOB headers ${CMAKE_CURRENT_SOURCE_DIR}/include/CSV/*)
#install(FILES ${headers}
#  DESTINATION include/CSV
#  )

install(DIRECTORY include/FCKinFitter
  DESTINATION include
  )


include(GNUInstallDirs)
install(TARGETS FCKinFitter
    EXPORT KinFitterTargets
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)


